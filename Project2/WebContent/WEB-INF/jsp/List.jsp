
<%@ page language="java" contentType="text/html; charset=UTF-88"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>



<meta charset="utf-8">


<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

</head>

<body>
	<nav class="navbar navbar-dark bg-dark">

		<a class="navbar-brand" href="#">ユーザ名さん</a>

		<p class="text-danger">ログアウト</p>

	</nav>
	<h1 class="text-center">ユーザ一覧</h1>


	<p class="text-right">
		<a class="nav-link active" href="#">新規登録</a>
	</p>


	<!-- /header -->

	<!-- body -->

	<div class="container">
		<div class="row">
			<div class="col-6 col-sm-2">ログインID</div>
			<div class="col-6 col-sm-4">
				<input type="text" style="width: 400px;" name="loginId"
					id="inputLoginId" class="form-control" required autofocus>
			</div>


			<!-- Force next columns to break to new line -->
			<div class="w-100"></div>

			<div class="col-6 col-sm-2">ユーザー名</div>
			<div class="col-6 col-sm-4">
				<input type="text" style="width: 400px;" name="loginId"
					id="inputLoginId" class="form-control" required autofocus>
			</div>

			<div class="container">
				<div class="row">

					<div class="col-12 col-sm-2">生年月日</div>
					<div class="col-12 col-sm-2">
						<input type="date" style="width: 150px;" name="loginId"
							id="inputLoginId">
					</div>
					<div class="col-12 col-sm-1">～</div>
					<div class="col-12col-sm-4">
						<input type="date" style="width: 150px;" name="loginId"
							　
                            id="inputLoginId"
							class="form-control" required autofocus>


					</div>


				</div>
				<p class="text-right">

					<button type="button" class="btn btn-secondary btn-sm">検索</button>
				</p>


				<table class="table table-bordered">
					<thead class="table-dark">


						<tr>
							<th scope="col">ログインID</th>
							<th scope="col">ユーザー名</th>
							<th scope="col">生年月日</th>
							<th scope="col"></th>
						</tr>
					</thead>
					<tbody>
						        <c:forEach var="user" items="${userList}" >
                   <tr>
                     <td>${user.loginId}</td>
                     <td>${user.name}</td>
                     <td>${user.birthDate}</td>
                     <!-- TODO 未実装；ログインボタンの表示制御を行う -->
                     <td>
                       <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
                       <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
                       <a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
                     </td>
                   </tr>
                 </c:forEach>

					</tbody>
				</table>
</body>

</html>