import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SelectUserInformation {
	public static void main(String[] args) {
		Connection conn = null;
		try {

			// データベースへ接続
			conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/login?useUnicode=true&characterEncoding=utf8", "root", "password");

			// SELECT文を準備
			String sql = "SELECT id, name FROM userinformation";

			// SELECTを実行し、結果表（ResultSet）を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				String id = rs.getString("id");
				String name = rs.getString("name");

				// 取得したデータを出力
				System.out.println("ID:" + id);
				System.out.println("名前:" + name);

			}
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
