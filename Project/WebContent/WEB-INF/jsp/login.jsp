<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>


<meta charset="utf-8">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

</head>

<body>
<h1 class="text-center">ログイン画面</h1>

	<form class="form-signin" action="LoginServlet" method="post">
		<div class="row">

			<div class="col-6 col-sm-2">ログインID</div>
			<div class="col-6 col-sm-4">
				<input type="text" style="width: 200px;" name="loginId"
					id="inputLoginId" class="form-control" required autofocus>
			</div>

			<!-- Force next columns to break to new line -->
			<div class="w-100"></div>

			<div class="col-6 col-sm-2">ユーザー名</div>
			<div class="col-6 col-sm-4">
				<input type="text" style="width: 200px;" name="userName"
					id="inputLoginId" class="form-control" required autofocus>
			</div>
		</div>

		<p class="text-center">
			<button type="submit" class="btn btn-secondary btn-sm">ログイン</button>
		</p>
	</form>



</body>

</html>