<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>


<meta charset="utf-8">


<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

</head>

<body>

	<nav class="navbar navbar-dark bg-dark">

		<ul class="nav navbar-nav navbar-right">
			<li class="navbar-text">${userInfo.name}さん</li>
			<li class="dropdown"><a href="LogoutServlet"
				class="navbar-link logout-link">ログアウト</a></li>
		</ul>

	</nav>
	<h1 class="text-center">ユーザ更新</h1>

	<div class="row">
		<div class="col">ログインID</div>
		<div class="col">${user.loginId}</div>
		<div class="col"></div>
	</div>
	<div class="row">
		<div class="col">パスワード</div>
		<div class="col">
			<div class="col-6 col-sm-4">
				<input type="text" style="width: 200px;" name="password"
					id="inputLoginId" class="form-control" required autofocus>
			</div>
		</div>
		<div class="col"></div>
	</div>
	<div class="row">
		<div class="col">パスワード（確認）</div>
		<div class="col">
			<div class="col-6 col-sm-4">
				<input type="text" style="width: 200px;" name="password2"
					id="inputLoginId" class="form-control" required autofocus>
			</div>
		</div>
		<div class="col"></div>
	</div>
	<div class="row">
		<div class="col">ユーザー名</div>
		<div class="col">
			<div class="col-6 col-sm-4">
				<input type="text" style="width: 200px;" name="userName"
					id="inputLoginId" value="${user.name}" class="form-control"
					required autofocus>
			</div>
		</div>
		<div class="col"></div>
	</div>
	<div class="row">
		<div class="col">生年月日</div>
		<div class="col">
			<div class="col-6 col-sm-4">
				<input type="text" style="width: 200px;" name="birthDate"
					id="inputLoginId" value="${user.birthDate}" class="form-control"
					required autofocus>
			</div>
		</div>
		<div class="col"></div>
	</div>

	<p class="text-center">
		<button type="button" class="btn btn-secondary btn-sm">更新</button>
	</p>

	<nav class="nav">
		<a class="nav-link active" href="#" onclick="history.back()">戻る</a>
	</nav>
</body>

</html>