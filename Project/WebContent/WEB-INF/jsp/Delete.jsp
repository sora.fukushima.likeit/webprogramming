<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>


<meta charset="utf-8">



<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
	integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO"
	crossorigin="anonymous">

</head>

<body>

	<nav class="navbar navbar-dark bg-dark">

		<ul class="nav navbar-nav navbar-right">
			<li class="navbar-text">${userInfo.name}さん</li>
			<li class="dropdown"><a href="LogoutServlet"
				class="navbar-link logout-link">ログアウト</a></li>
		</ul>

	</nav>
	<h1 class="text-center">ユーザ削除確認</h1>


	<div class="row">
		<div class="col">ログインID:${userInfo.loginId}</div>
	</div>
	<div class="row">
		<div class="col">を本当に削除してよろしいでしょよろしいでしょうか。</div>
	</div>
	<p class="text-center">
		<button type="button" class="btn btn-secondary btn-sm" onclick="history.back()">キャンセル
		</button>
		<button type="button" class="btn btn-secondary btn-sm">OK</button>
	</p>

</body>

</html>